import express from 'express';

export const router = express.Router();

// Middleware para parsear el cuerpo de las peticiones
router.use(express.json());
router.use(express.urlencoded({ extended: true }));

router.get('/', (req, res) => {
    res.render('index', {titulo: "Colegio Patria", nombre: "Ontiveros Govea Yair Alejandro"});
});


router.post('/salida', (req, res) => {
    const { nombre, nivel, pagoBase, horas, numHijos } = req.body;

    // Convertir a números
    const nivelNumerico = parseInt(nivel, 10);
    const pagoBaseNumerico = parseFloat(pagoBase);
    const horasNumerico = parseInt(horas, 10);
    const numHijosNumerico = parseInt(numHijos, 10);

    // Calcular el incremento según el nivel
    let incremento = 0;
    switch (nivelNumerico) {
        case 1:
            incremento = 0.30; // 30%
            break;
        case 2:
            incremento = 0.50; // 50%
            break;
        case 3:
            incremento = 1.00; // 100%
            break;
        default:
            // Manejar error o caso no esperado
            break;
    }

    // Calcular el pago base con incremento por horas trabajadas
    const pagoConIncremento = pagoBaseNumerico * (1 + incremento) * horasNumerico;

    // Calcular el bono según el número de hijos
    let porcentajeBono = 0;
    if (numHijosNumerico >= 1 && numHijosNumerico <= 2) {
        porcentajeBono = 0.05; // 5%
    } else if (numHijosNumerico >= 3 && numHijosNumerico <= 5) {
        porcentajeBono = 0.10; // 10%
    } else if (numHijosNumerico > 5) {
        porcentajeBono = 0.20; // 20%
    }

    const bono = pagoConIncremento * porcentajeBono;

    const impuesto = pagoConIncremento * 0.16;

    const total = pagoConIncremento + bono - impuesto;

    res.render('salida', {
        nombre,  
        pagoBase: pagoConIncremento.toFixed(2), 
        pagoBono: bono.toFixed(2), 
        impuesto: impuesto.toFixed(2), 
        total: total.toFixed(2)
    });    
});

export default {router}